package ua.com.poseal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.com.poseal.data.Data;
import ua.com.poseal.dto.LeftoverDTO;
import ua.com.poseal.util.Loader;
import ua.com.poseal.util.MongoDBExecutor;

import java.util.Properties;

import static ua.com.poseal.util.Loader.CATEGORY;

public class App {
    public static final Logger logger = LoggerFactory.getLogger("LOGGER");
    private static final String PRODUCTS = "products";

    public static void main(String[] args) {
        logger.debug("Start program");
        try {
            App app = new App();
            app.run();
        } catch (Exception e) {
            logger.error("Error running program", e);
        }
        logger.debug("Stop program");
    }

    private void run() {
        logger.debug("Entered run() method");
        // Load properties
        Loader loader = new Loader();
        Properties properties = loader.getFileProperties();
        // Load data from files
        Data data = loader.loadDataFromFiles();

        MongoDBExecutor executor = new MongoDBExecutor(properties, data);
        executor.createCollections();
        executor.insertDataToCollections();
        // Generate products and insert them in the collection
        executor.saveProducts(Long.parseLong(properties.getProperty(PRODUCTS)));
        // Fill leftover collection
        executor.saveLeftover();

        executor.createIndexes();
        // query task
        LeftoverDTO dto = executor.findAddressByCategory(properties.getProperty(CATEGORY));
        logger.info(
                "The address of the store with the largest number of products in the category \"{}\": {}, count = {}",
                properties.getProperty(CATEGORY), dto.getAddress(), dto.getAmount());

        executor.close();

        logger.debug("Exited run() method");
    }
}
